

import 'package:flutter_test/flutter_test.dart';
import 'package:todoapp/tasks/control/JsonConverter.dart';
import 'package:todoapp/tasks/entity/TaskEntity.dart';
import 'package:todoapp/tasks/entity/VisionEntity.dart';

void main(){
  var jsonConverter = JsonConverter();

  test("should_convert_vision_to_string", (){
    List<VisionEntity> root = [VisionEntity(1, "fsd"),VisionEntity(2, "two")];
    expect(jsonConverter.visionListToString(root), equals("{\"id\":1,\"title\":\"fsd\"};{\"id\":2,\"title\":\"two\"}"));
  });

  test("should_convert_string_to_vision", (){
    var list = jsonConverter.stringToVisionList("{\"id\":1,\"title\":\"fsd\"};{\"id\":2,\"title\":\"two\"}");
    expect(list[0].title, equals("fsd"));
    expect(list[1].title, equals("two"));
  });

  test("should_convert_task_to_string", (){
    var taskEntity = TaskEntity(6, 1, "two");
    taskEntity.parentId = 5;
    List<TaskEntity> root = [TaskEntity(5, 1, "fsd"),taskEntity];
    expect(jsonConverter.taskListToString(root), equals('{"id":5,"visionId":1,"parentId":null,"title":"fsd"};{"id":6,"visionId":1,"parentId":5,"title":"two"}'));
  });

  test("should_convert_string_to_task", (){
    List<TaskEntity> list = jsonConverter.stringToTaskList('{"id":5,"visionId":1,"parentId":null,"title":"fsd"};{"id":6,"visionId":1,"parentId":5,"title":"two"}');
    expect(list[0].title, equals("fsd"));
    expect(list[1].title, equals("two"));
  });
/*
  test("should_load_vision", (){
    List<VisionEntity> root = [VisionEntity(1, "fsd")];
    List<VisionEntity> res = json.decode("[{\"id\":1,\"title\":\"fsd\"}]");
    expect(root, equals(res));
  });

  test("should_store_task", (){
    RootEntity root = RootEntity([], [TaskEntity(2, 1, "myTitle")]);
    expect(root.toJSON(), equals("{visions: [], tasks: [{\"id\":2,\"visionId\":1,\"parentId\":null,\"title\":\"myTitle\"}]}"));
  });

 */
}