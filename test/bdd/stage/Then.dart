import 'package:flutter_test/flutter_test.dart';
import 'package:todoapp/App.dart';

import '../mock/RootRepositoryMock.dart';

class Then{
  App app;
  Then(this.app);

  vision_with_title(String title){
    var visionPage = app.loadVisionPage.execute();
    expect(visionPage[0].title, title);
  }

  void vision_entry_has_task(String title) {
    var visionPage = app.loadVisionPage.execute();
    expect(visionPage.length, 1);
    var task = visionPage[0].tasks.where((element) => element.title == title).first;
    expect(task.title, title);
  }
}