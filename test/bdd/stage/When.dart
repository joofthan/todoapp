import 'package:flutter_test/flutter_test.dart';
import 'package:todoapp/App.dart';
import 'package:todoapp/tasks/boundary/LoadVisionPage.dart';

import '../mock/RootRepositoryMock.dart';

class When{
  App app;
  When(this.app);

  When vision_with_task(String vision, String taskTitle) {
    app.addVisionEntry.execute(vision);
    List<VisionEntry> visions = app.loadVisionPage.execute();
    expect(visions.length, 1);
    app.addTaskEntry.execute(visions[0].id, null, taskTitle);
    return this;
  }

  When vision_with_task_and_subtask(String vision, String taskTitle, String subtask) {
    vision_with_task(vision, taskTitle);
    List<VisionEntry> visions = app.loadVisionPage.execute();
    var tasks = visions[0].tasks;
    expect(tasks, 1);
    app.addTaskEntry.execute(visions[0].id, tasks[0].id, taskTitle);
    return this;
  }

/*
  Given vision(String title){
    _loadVisionPage = LoadVisionPage(VisionRepository([VisionEntity(1,title)],[]));
    return this;
  }

  load_vision_page(){
    visionPage = _loadVisionPage?.execute();
  }


  vision_with_title(String title){
    expect(visionPage?[0].title, title);
  }

  void vision_entry_has_task(String t) {
    expect(visionPage?[0].tasks[0].title, t);
  }
*/
}