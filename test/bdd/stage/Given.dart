import 'package:flutter_test/flutter_test.dart';
import 'package:todoapp/App.dart';
import 'package:todoapp/tasks/boundary/LoadVisionPage.dart';

import '../mock/RootRepositoryMock.dart';

class Given{
  App app;
  Given(this.app);

  Given vision_with_task(String vision, String taskTitle) {
    app.addVisionEntry.execute(vision);
    List<VisionEntry> visions = app.loadVisionPage.execute();
    expect(visions.length, 1);
    app.addTaskEntry.execute(visions[0].id, null, taskTitle);
    return this;
  }

  Given vision_with_task_and_subtask(String vision, String taskTitle, String subtask) {
    vision_with_task(vision, taskTitle);
    List<VisionEntry> visions = app.loadVisionPage.execute();
    var tasks = visions[0].tasks;
    expect(tasks.length, 1);
    app.addTaskEntry.execute(visions[0].id, tasks[0].id, subtask);
    return this;
  }


  Given vision(String title){
    app.addVisionEntry.execute(title);
    return this;
  }
}