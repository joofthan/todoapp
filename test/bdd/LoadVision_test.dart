import 'package:flutter_test/flutter_test.dart';
import 'package:todoapp/App.dart';

import 'mock/RootRepositoryMock.dart';
import 'stage/Given.dart';
import 'stage/Then.dart';
import 'stage/When.dart';

DatabaseMock db = DatabaseMock();
App app = App.createMock(db);

Given given = Given(app);
When when = When(app);
Then then = Then(app);

void main() {
  setUp(() => {db.reset()});

  test("should_load_vision_page", () {
    given.vision("Hello");
    //when.load_vision_page();
    then.vision_with_title("Hello");
  });

  test("should_load_vision_page_with_task", () {
    given.vision_with_task("Hello", "Task1");
    //when.load_vision_page();
    then.vision_entry_has_task("Task1");
  });

  test("should_load_vision_page_with_task_and_subtask", () {
    given.vision_with_task_and_subtask("Hello", "Task1", "SubTask");
    //when.load_vision_page();
    then.vision_entry_has_task("SubTask");
  });
}
