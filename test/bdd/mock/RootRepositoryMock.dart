import 'package:todoapp/database/Database.dart';

class DatabaseMock implements Database{

  Map<String,String> map = {};

  @override
  String load(String table) {
    var result = map[table];
    if(result == null){
      return "";
    }
    return result;
  }

  @override
  void save(String table, String data) {
    map[table] = data;
  }

  void reset() {
    map.clear();
  }

}