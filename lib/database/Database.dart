import 'package:shared_preferences/shared_preferences.dart';

class Database{
  SharedPreferences _pref;
  int _version;
  Database(this._version, this._pref);

  String load(String table){
    var res = _pref.getString(table+"v"+_version.toString());
    if(res == null){
      return "";
    }
    print("Loading "+table + ": "+res);
    return res;
  }

  void save(String table, String data){
    _pref.setString(table+"v"+_version.toString(), data);
  }
}