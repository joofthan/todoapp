import 'package:shared_preferences/shared_preferences.dart';
import 'package:todoapp/tasks/boundary/AddTaskEntry.dart';
import 'package:todoapp/tasks/boundary/DeleteTask.dart';
import 'package:todoapp/tasks/boundary/LoadTaskDetails.dart';
import 'package:todoapp/tasks/boundary/LoadTasksPage.dart';
import 'package:todoapp/tasks/boundary/AddVisionEntry.dart';
import 'package:todoapp/tasks/boundary/LoadVisionPage.dart';

import 'database/Database.dart';
import 'tasks/control/TaskRepository.dart';

class App{
  Database db;

  LoadVisionPage loadVisionPage;
  AddVisionEntry addVisionEntry;
  LoadTasksPage loadTasksPage;
  AddTaskEntry addTaskEntry;
  LoadTaskDetails loadTaskDetails;
  DeleteTask deleteTask;

  App(this.db):
        loadVisionPage = LoadVisionPage(TaskRepository(db)),
        addVisionEntry = AddVisionEntry(TaskRepository(db)),
        loadTasksPage = LoadTasksPage(TaskRepository(db)),
        loadTaskDetails = LoadTaskDetails(TaskRepository(db)),
        addTaskEntry = AddTaskEntry(TaskRepository(db)),
        deleteTask = DeleteTask(TaskRepository(db))
  ;

  static Future<App> create() async {
    return App(Database(1, await SharedPreferences.getInstance()));
  }

  static App createMock(Database db){
    return App(db);
  }
}

