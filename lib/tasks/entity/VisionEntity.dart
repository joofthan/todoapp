
class VisionEntity {
  int id;
  String title;

  VisionEntity(this.id, this.title);

  VisionEntity.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
    };
  }
}