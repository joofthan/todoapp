class TaskEntity {
  int id;
  int visionId;
  int? parentId;
  String title;

  TaskEntity(this.id,this.visionId, this.title);

  TaskEntity.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        visionId = json['visionId'],
        parentId = json['parentId'],
        title = json['title'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'visionId': visionId,
      'parentId': parentId,
      'title': title,
    };
  }
}