import 'dart:convert';



import 'TaskEntity.dart';
import 'VisionEntity.dart';

class RootEntity{
  List<VisionEntity> visions;
  List<TaskEntity> tasks;

  RootEntity(this.visions,this.tasks);

  toJSON() {
    return {
      'visions': json.encode(visions),
      'tasks': json.encode(tasks),
    };
  }

  RootEntity.fromJson(Map<String, dynamic> json)
      : visions = json['visions'],
        tasks = json['tasks'];

}