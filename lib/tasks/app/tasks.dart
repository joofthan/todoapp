import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:todoapp/tasks/boundary/LoadTasksPage.dart';
import 'package:todoapp/tasks/app/TaskButton.dart';

import '../../../App.dart';
import '../control/SelectedVision.dart';
import '../entity/VisionEntity.dart';

class TasksWidget extends StatefulWidget {
  SelectedVision selectedVision;
  TasksWidget(void Function(int id) goToVision, this.selectedVision, {Key? key}) : super(key: key);

  @override
  State<TasksWidget> createState() => _TasksWidgetState(selectedVision);
}

class _TasksWidgetState extends State<TasksWidget> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  Future<App> app = Future(()async {return await App.create();});
  SelectedVision? selectedVision;

  _TasksWidgetState(SelectedVision s){
    selectedVision = s;
  }

  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      //_selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(margin: EdgeInsets.all(10), child: FutureBuilder<App>(future: app,builder: (BuildContext context, AsyncSnapshot<App> snapshot){
        if(snapshot.hasData){
          TasksPage? page = snapshot.data?.loadTasksPage.execute(this.selectedVision?.id);
          if(this.selectedVision?.id == null){
            selectedVision?.id = page?.selected;
          }
          if(page == null){
            return Text("Error: page == null");
          }
          return Wrap(children:[
            DropdownButton<int>(
            value: selectedVision?.id,
            icon: const Icon(Icons.arrow_downward),
            elevation: 16,
            style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (int? newValue) {
              setState(() {
                selectedVision?.id = newValue;
              });
            },
            items: page.dropdown
                .map<DropdownMenuItem<int>>((VisionEntity value) {
              return DropdownMenuItem<int>(
                value: value.id,
                child: Text(value.title),
              );
            }).toList(),
          ),
            ListBody(
              children: page.tasks.map((e) => TaskButton(e.id, e.title)).toList(),
            )
            //Column(children: loadVisionPage.execute().map((e) => VisionGoalEntry(e)).toList())
          ]
          );
        }else{
          return Text("Loading");
        }
      }))
    ],);
  }
}
