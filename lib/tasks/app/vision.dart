import 'package:flutter/material.dart';

import '../../App.dart';
import '../boundary/LoadVisionPage.dart';
import 'VisionGoalEntry.dart';

class VisionWidget extends StatefulWidget {
  void Function(int id) goToVision;

  VisionWidget(this.goToVision, {Key? key}) : super(key: key);

  @override
  State<VisionWidget> createState() => _VisionWidgetState(goToVision);
}

class _VisionWidgetState extends State<VisionWidget> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  Future<App> app = Future(()async {return await App.create();});
  void Function(int id) goToVision;

  _VisionWidgetState(this.goToVision);

  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      //_selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(margin: EdgeInsets.all(10), child: FutureBuilder<App>(future: app,builder: (BuildContext context, AsyncSnapshot<App> snapshot){
        if(snapshot.hasData){
          LoadVisionPage? loadVisionPage = snapshot.data?.loadVisionPage;
          if(loadVisionPage == null){
            return Text("Error: loadVisionPage == null");
          }
          return Wrap(children:[
            Text("Welche langfristigen Ziele möchtest du erreichen?", textScaleFactor: 1.3,textAlign: TextAlign.center,),
            Column(children: loadVisionPage.execute().map((e) => VisionGoalEntry(e, this.goToVision)).toList())
          ]
          );
        }else{
          return Text("Loading");
        }
      }))
    ],);
  }
}
