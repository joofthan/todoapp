import 'package:flutter/material.dart';
import 'package:todoapp/tasks/app/tasks.dart';
import 'package:todoapp/tasks/app/vision.dart';
import 'package:todoapp/tasks/boundary/DeleteTask.dart';

import '../../App.dart';
import '../control/SelectedVision.dart';



class TaskDetailsView extends StatefulWidget {
  int id;
  TaskDetailsView( this.id, {Key? key}) : super(key: key);

  @override
  State<TaskDetailsView> createState() => _TaskDetailsViewState();
}

class _TaskDetailsViewState extends State<TaskDetailsView> {
  final myController = TextEditingController();

  Future<App> app = Future(() async {return await App.create();});

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  void initState(){
    super.initState();
    app.then((value) => myController.text = value.loadTaskDetails.execute(widget.id).title);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Task Details'),
      ),
      body: FutureBuilder<App>(future: app, builder: (BuildContext context, AsyncSnapshot<App> snapshot){
        return Container(margin: EdgeInsets.all(10), child:Column(children: [
          //Text("Task: "+(snapshot.data?.loadTaskDetails.execute(widget.id).title)!, textScaleFactor: 1.3,textAlign: TextAlign.center,),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: TextField(
              controller: myController,
              autofocus: true,
              decoration: InputDecoration(
                  hintText: "Title",
                  border: OutlineInputBorder()
              ),)
            ,)
          ,
          ElevatedButton(child: Text("Unteraufgaben erstellen"),onPressed: () {throw Exception("Not implemented");},),
          ElevatedButton(child: Text("Als Tagesziel festlegen"),onPressed: () {throw Exception("Not implemented");},),
          ElevatedButton(child: Text("Erledigt"),onPressed: () {throw Exception("Not implemented");},),
          ElevatedButton(style: ElevatedButton.styleFrom(primary: Colors.red), child: Text("Löschen"),
            onPressed: () async {
              (await app).deleteTask.execute(widget.id);
              Navigator.pop(context);
            },
          ),
          ElevatedButton(child: Text("Speichern"),onPressed: () async {
            (await app).loadTaskDetails.execute(widget.id);
            Navigator.pop(context);
          },)
        ],),
        );
      },));
  }
}