import 'package:flutter/material.dart';

import '../../App.dart';



class AddVisionView extends StatefulWidget {
  AddVisionView({Key? key}) : super(key: key);

  @override
  State<AddVisionView> createState() => _AddVisionViewState();
}

class _AddVisionViewState extends State<AddVisionView> {
  final myController = TextEditingController();

  Future<App> app = Future(() async {return await App.create();});

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Vision anlegen'),
      ),
      body: Container(margin: EdgeInsets.all(10), child:Column(children: [
        Text("Welche langfristigen Ziele möchtest du erreichen?", textScaleFactor: 1.3,textAlign: TextAlign.center,),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: TextField(
            controller: myController,
            autofocus: true,
            decoration: InputDecoration(
            hintText: "Ziel",
            border: OutlineInputBorder()
        ),)
          ,)
        ,
        ElevatedButton(child: Text("Fertig"),onPressed: () async {
          (await app).addVisionEntry.execute(myController.value.text);
          Navigator.pop(context);
        },)
      ],),
    ));
  }
}