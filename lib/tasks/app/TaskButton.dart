import 'package:flutter/material.dart';
import 'package:todoapp/tasks/app/TaskDetailsView.dart';

class TaskButton extends StatelessWidget{
  int id;
  String text;
  TaskButton(this.id, this.text);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailsView(id))),
        child:Container(
      margin: EdgeInsets.only(left: 40, right: 30),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(left: BorderSide(),right: BorderSide(),bottom: BorderSide(),)
      ),
      child: Center(child: Text(text),),
    )
    );
  }

}