import 'package:flutter/material.dart';

import '../boundary/LoadVisionPage.dart';
import 'TaskButton.dart';

class VisionGoalEntry extends StatelessWidget {

  VisionEntry visionEntry;
  void Function(int id) goToVision;
  VisionGoalEntry(this.visionEntry, this.goToVision);


  @override
  Widget build(BuildContext context) {
    return Wrap(children: [
      GestureDetector(
          onTap: ()=> {this.goToVision(visionEntry.id)},
          child: Container(
            height: 50,
            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromRGBO(10, 10, 10, 5),
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Center(child: Text(visionEntry.title)),
          )),
      ListBody(
        children: visionEntry.tasks.map((e) => TaskButton(e.id, e.title)).toList(),
      )
    ]);
  }
}
