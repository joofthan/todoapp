import 'package:flutter/material.dart';

import '../../App.dart';
import '../control/SelectedVision.dart';



class AddTaskView extends StatefulWidget {
  int visionId;
  SelectedTaskParent selectedTaskParent;
  AddTaskView(this.visionId, this.selectedTaskParent, {Key? key}) : super(key: key);

  @override
  State<AddTaskView> createState() => _AddTaskViewState();
}

class _AddTaskViewState extends State<AddTaskView> {
  final myController = TextEditingController();

  Future<App> app = Future(() async {return await App.create();});

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Task anlegen'),
      ),
      body: Container(margin: EdgeInsets.all(10), child:Column(children: [
        Text("Was kannst du tun um das Ziel zu erreichen?", textScaleFactor: 1.3,textAlign: TextAlign.center,),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: TextField(
            controller: myController,
            autofocus: true,
            decoration: InputDecoration(
            hintText: "Aufgabe",
            border: OutlineInputBorder()
        ),)
          ,)
        ,
        ElevatedButton(child: Text("Fertig"),onPressed: () async {
          (await app).addTaskEntry.execute(widget.visionId, widget.selectedTaskParent.id , myController.value.text);
          Navigator.pop(context);
        },)
      ],),
    ));
  }
}