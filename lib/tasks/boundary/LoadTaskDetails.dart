import '../control/TaskRepository.dart';
import '../entity/TaskEntity.dart';

class LoadTaskDetails {
  TaskRepository _taskRepository;
  LoadTaskDetails(this._taskRepository);

  TaskEntity execute(int? taskId){
    if(taskId == null){
      throw Exception("Task not found: TaskId is null");
    }
    return _taskRepository.findById(taskId);
  }
}