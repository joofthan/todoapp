
import '../control/TaskRepository.dart';
import '../entity/TaskEntity.dart';

class AddTaskEntry{
  TaskRepository _rootRepository;
  AddTaskEntry(this._rootRepository);
  execute(int visionId, int? parentId, String name){
    var taskEntity = TaskEntity(_rootRepository.generateId(), visionId , name);
    taskEntity.parentId = parentId;
    _rootRepository.addTask(taskEntity);
    //_rootRepository.addTask(TaskEntity(_rootRepository.generateId(), visionEntity.id, "AddVisionEntry test"));
  }
}