import 'package:todoapp/tasks/control/TaskRepository.dart';

import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';

class DeleteTask{
  TaskRepository rootRepository;
  DeleteTask(this.rootRepository);

  void execute(int? taskId){
    if(taskId == null){
      throw Exception("Cannot delete Task with id null");
    }
    rootRepository.deleteTask(taskId);
  }
}

class TasksPage{
  int selected;
  List<VisionEntity> dropdown;
  List<TaskEntity> tasks;

  TasksPage(this.selected,this.dropdown, this.tasks);
}