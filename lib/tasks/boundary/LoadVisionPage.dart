

import '../control/TaskRepository.dart';
import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';

class LoadVisionPage{
  TaskRepository _visionRepository;

  LoadVisionPage(this._visionRepository);

  List<VisionEntry> execute() {
    List<VisionEntity> x = _visionRepository.findAll();

    var list = List.of(x.map((e) => VisionEntry(e.id, e.title, tasksFor(e.id))));
    return list;
  }

  List<TaskEntry> tasksFor(int id) {
    List<TaskEntity> tasks = _visionRepository.findTasksByVisionId(id);
    return List.of(tasks.map((e) => TaskEntry(e.id,e.title)));
  }
}

class VisionEntry{
  int id;
  String title;
  List<TaskEntry> tasks;

  VisionEntry(this.id,this.title, this.tasks);
}

class TaskEntry {
  int id;
  String title;
  List<TaskEntry> subTasks = [];

  TaskEntry(this.id, this.title);
}