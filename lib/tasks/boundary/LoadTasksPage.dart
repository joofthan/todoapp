import 'package:todoapp/tasks/control/TaskRepository.dart';

import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';

class LoadTasksPage{
  TaskRepository rootRepository;
  LoadTasksPage(this.rootRepository);

  TasksPage execute(int? selected){
    var drop = rootRepository.findAll();
    selected ??= drop[0].id;
    var findTasksByVisionId = rootRepository.findTasksByVisionId(selected);
    return TasksPage(selected, drop, findTasksByVisionId);
  }
}

class TasksPage{
  int selected;
  List<VisionEntity> dropdown;
  List<TaskEntity> tasks;

  TasksPage(this.selected,this.dropdown, this.tasks);
}