import '../control/TaskRepository.dart';
import '../entity/VisionEntity.dart';

class AddVisionEntry{
  TaskRepository _rootRepository;
  AddVisionEntry(this._rootRepository);
  execute(String name){
    var visionEntity = VisionEntity(_rootRepository.generateId(), name);
    _rootRepository.addVision(visionEntity);
    //_rootRepository.addTask(TaskEntity(_rootRepository.generateId(), visionEntity.id, "AddVisionEntry test"));
  }
}