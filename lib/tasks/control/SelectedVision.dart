

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../database/Database.dart';
import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';


class SelectedVision{
  int? id;
}

class SelectedTaskParent{
  int? id;
}