import 'dart:convert';

import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';

class JsonConverter {

  List<VisionEntity> stringToVisionList(String data){
    List<String> c = data.split(";");
    List<VisionEntity> li = [];
    for(int i = 0 ; i < c.length; i++){
      li.add(VisionEntity.fromJson(json.decode(c[i])));
    }

    return li;
  }

  String visionListToString(List<VisionEntity> root) {
    String result = json.encode(root[0]);

    for(int i = 1 ; i < root.length; i++){
      result += ";" + json.encode(root[i]);
    }
    return result;
  }

  String taskListToString(List<TaskEntity> root) {
    String result = json.encode(root[0]);

    for(int i = 1 ; i < root.length; i++){
      result += ";" + json.encode(root[i]);
    }
    return result;
  }

  List<TaskEntity> stringToTaskList(String data) {
    List<String> c = data.split(";");
    List<TaskEntity> li = [];
    for(int i = 0 ; i < c.length; i++){
      li.add(TaskEntity.fromJson(json.decode(c[i])));
    }

    return li;
  }
}