

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../entity/TaskEntity.dart';
import '../entity/VisionEntity.dart';
import '../../database/Database.dart';
import 'JsonConverter.dart';

class TaskRepository{
  Database db;
  JsonConverter jsonConverter = JsonConverter();
  TaskRepository(this.db);

  static final  validCharacters = RegExp(r'^[a-zA-Z0-9 ]+$');

  int generateId(){
    return DateTime.now().millisecondsSinceEpoch;
  }

  void addVision(VisionEntity vision){
    validate(vision.title);
    List<VisionEntity> li = [];
    li.addAll(findAll());
    li.add(vision);
    db.save("vision", jsonConverter.visionListToString(li));
  }

  void addTask(TaskEntity task){
    validate(task.title);
    List<TaskEntity> li = [];
    li.addAll(findAllTasks());
    li.add(task);
    db.save("task", jsonConverter.taskListToString(li));
  }

  validate(String value){
    if(!validCharacters.hasMatch(value)){
      throw Exception("Invalid characters: "+value);
    }
  }

  List<VisionEntity> findAll() {
    
    String jsonString = db.load("vision");
    if(jsonString.isEmpty){
      return List.empty();
    }
    List<VisionEntity> li = jsonConverter.stringToVisionList(jsonString);

    return li;
  }

  List<TaskEntity> findAllTasks() {
    String jsonString = db.load("task");
    if(jsonString.isEmpty){
      return List.empty();
    }
    List<TaskEntity> li = jsonConverter.stringToTaskList(jsonString);

    return li;
  }


  List<TaskEntity> findTasksByVisionId(int id) {
    List<TaskEntity> tasks = findAllTasks();
    if(tasks.isEmpty){
      return List.empty();
    }
    return List.of(
        tasks.where((element) => element.visionId == id)
    );
  }

  TaskEntity findById(int taskId) {
    return findAllTasks().where((element) => element.id == taskId).first;
  }

  void deleteTask(int taskId) {
    List<TaskEntity> li = [];
    li.addAll(findAllTasks());
    li.removeWhere((element) => element.id == taskId);
    db.save("task", jsonConverter.taskListToString(li));
  }

  void deleteVision(int visionId) {
    List<VisionEntity> li = [];
    li.addAll(findAll());
    li.removeWhere((element) => element.id == visionId);
    db.save("vision", jsonConverter.visionListToString(li));
  }

}