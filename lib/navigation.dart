import 'package:flutter/material.dart';
import 'package:todoapp/tasks/app/AddTaskView.dart';
import 'package:todoapp/tasks/app/tasks.dart';
import 'package:todoapp/tasks/control/SelectedVision.dart';

import 'App.dart';
import 'home/app/home.dart';
import 'tasks/app/AddVisionView.dart';
import 'tasks/app/vision.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  SelectedVision selectedVision = SelectedVision();
  SelectedTaskParent selectedTaskParent = SelectedTaskParent();

  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void goToVision(int id){
    setState(() {
      _selectedIndex = 2;
      selectedVision.id = id;
    });
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
      appBar: AppBar(
        title: Text('Todo App'),
      ),
      body: [
        HomeWidget(),
        VisionWidget(goToVision),
        TasksWidget(goToVision, selectedVision),
        Text(
          'Verlauf',
          style: optionStyle,
        ),
      ].elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flag_outlined), //map   visibility  place flag
            label: 'Vision',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Aufgaben',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history_toggle_off), // history_toggle_off   history_edu_outlined
            label: 'Verlauf',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),

      floatingActionButton: floatingActionButton(context)
        ,
    );
  }

  FloatingActionButton? floatingActionButton(BuildContext context) {
    if(_selectedIndex == 1){
      return FloatingActionButton(onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddVisionView()),
        ).then((value) => setState((){}));
      }, child: const Icon(Icons.add));
    }

    if(_selectedIndex == 2){
      return FloatingActionButton(onPressed: () {
        var id = selectedVision.id;
        if(id == null){
          return null;
        }
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddTaskView(id, selectedTaskParent)),
        ).then((value) => setState((){}));
      }, child: const Icon(Icons.add));
    }

    return null;
  }
}
