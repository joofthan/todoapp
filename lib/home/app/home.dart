import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HomeWidget extends StatefulWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);


  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      //_selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {


    return TextButton(onPressed: (){
      SharedPreferences.getInstance().then((value) => {
        value.clear()
      });
    }, child: Text("clear", textScaleFactor: 2,));
  }
}
